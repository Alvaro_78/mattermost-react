import React, { useState, Fragment } from 'react'
import Button from './component/Button'
import Result from './component/Result';

function App() {

  const  [post, guardarPost] = useState(null)
  
  
  return (
    <Fragment>
      <Button 
        guardarPost={guardarPost}
      />
      {post ? <Result post={post}/> : null}
    </Fragment>
    )
}

export default App;
