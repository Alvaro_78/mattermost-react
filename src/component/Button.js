import React from 'react'
import "./button.css" 

const Button = ({guardarPost}) => {

    const buttonClicked = () => {

        var myHeardes = new Headers({
            'Authorization': 'Bearer e64fmwawnjgkid8xgkynbextqo',
        })

        var miInit = { method: 'GET',
                headers: myHeardes,
        }

        fetch('https://chat.devscola.org/api/v4/channels/4ffqiyqz9389z88axumq4tu7uy/posts', miInit)
            .then((response) => {
                return response.json();
            })
            .then((myJson) => {
                let lastPost = myJson.order[0]
                let lastMessage = myJson.posts[lastPost].message
                console.log(lastMessage)
                guardarPost(lastMessage)
            })
    }

        return(
            <div className="myButton" onClick={buttonClicked}>
                Capturar Post
            </div>
        )
}

export default Button